Feature:Restaurant- Verify transaction like Auth,Sale, Credit or Force with cvvResult and Status for Restaurant with test scope with\without AVS,refund,with/without CVV,credit,void,swipe/manual.


  @post-transaction-sale_manual-with-avs
  Scenario Outline: Verify Transaction  'Sale' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {   "tip": <tipamount>,   "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",    "cvv":"<cvv>",   "expiration": "1220"    }, "billing":{"zip":"85284"},},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response body path status should be <status>
    And response header content-type should be application/json
    And response body path cvvResult should be <cvvResult>
    Examples:
      | txnCode | amount | cardNum          | status   | cvvResult | cvv  | tipamount |
      | Sale    | 11.13  | 4012000098765439 | Approved | M         | 999  | 1.11      |
      | Sale    | 8.15   | 2223000048400011 | Approved | M         | 998  | 0         |
      | Sale    | 15.00  | 371449635392376  | Approved | M         | 9997 | 1.05      |


  @post-transaction-sale_manual-no-avs
  Scenario Outline: Verify Transaction  'Sale' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": { "tip": <tipamount>,  "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",    "cvv":"<cvv>",   "expiration": "1220"    }},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response body path status should be <status>
    And response header content-type should be application/json
    And response body path cvvResult should be <cvvResult>
    Examples:
      | txnCode | amount | cardNum          | status   | cvvResult | cvv | tipamount |
      | Sale    | 10.00  | 3055155515160018 | Approved | M         | 996 | 0         |
      | Sale    | 11.00  | 6011000993026909 | Approved | M         | 996 | 0         |
      | Sale    | 5.00   | 3530142019945859 | Approved | M         | 996 | 0.50      |


  @post-transaction-sale_manual_cvvMismatch-no-avs
  Scenario Outline: Verify Transaction  'Sale' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",    "cvv":"<cvv>",   "expiration": "1220"    }},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response body path status should be <status>
    And response header content-type should be application/json
    And response body path cvvResult should be <cvvResult>
    Then response body path message should be CVV2 MISMATCH
    Examples:
      | txnCode | amount | cardNum          | status   | cvvResult | cvv |
      | Sale    | 2.00   | 4012000098765439 | Declined | N         | 123 |


  @post-transaction-sale_swipe-with-avs
  Scenario Outline: Verify Transaction  'Sale' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": { "tip": <tipamount>,   "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "trackData":{    "value":"<trackvalue>",    "format":"ClearText",    "isContactless":false    },"billing":{"zip":"85284"}},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response body path status should be <status>
    And response header content-type should be application/json
    And response body path cvvResult should be <cvvResult>
    Examples:
      | txnCode | amount | trackvalue                                                        | status   | cvvResult | tipamount |
      | Sale    | 11.12  | B4788250000028291^CHASE PAYMENTECH^15121015432112345601           | Approved | P         | 1.10      |
      | Sale    | 11.10  | B5454545454545454^CHASE PAYMENTECH TEST CARD^20121015432112345601 | Approved | P         | 0         |
      | Sale    | 10.01  | B6011000995500000^ CHASE PAYMENTECH ^20121015432112345678         | Declined | P         | 0         |
      | Sale    | 7.05   | B371449635398431^CHASE PAYMENTECH ^20121015432112345678           | Approved | P         | 0         |

  @post-transaction-sale_swipe-no-avs
  Scenario Outline: Verify Transaction  'Sale' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": { "tip": <tipamount>,    "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "trackData":{    "value":"<trackvalue>",    "format":"ClearText",    "isContactless":false    }},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response body path status should be <status>
    And response header content-type should be application/json
    And response body path cvvResult should be <cvvResult>
    Examples:
      | txnCode | amount | trackvalue                                                | status   | cvvResult | tipamount |
      | Sale    | 6.00   | B36438999960016^ CHASE PAYMENTECH ^20121015432112345678   | Approved | P         | 1.00      |
      | Sale    | 10.00  | B6011000995500000^ CHASE PAYMENTECH ^20121015432112345678 | Approved | P         | 1.00      |
      | Sale    | 3.00   | B3566002020140006^CHASE PAYMENTECH^20121015432112345678   | Approved | P         | 0.45      |


  @post-transaction-force-with-avs
  Scenario Outline: 'Force' transaction for set of  card numbers,  and verify cvv2 and response
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {  "total": <amount>    },   "authorizationCode": "<AuthCode>",     "orderNumber": "5656",    "cardData": {   "number": "<CardNum>",     "expiration": "1220"     },"billing":{"zip":"85284"}},     "transactionCode": "Force"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be Approved
    And response body path cvvResult should be P
    Examples:
      | AuthCode | CardNum          | amount |
      | T12345   | 5499740000000057 | 6.67   |

  @post-transaction-force-no-avs
  Scenario Outline: 'Force' transaction for set of  card numbers,  and verify cvv2 and response
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": { "tip": <tipamount>, "total": <amount>    },   "authorizationCode": "<AuthCode>",     "orderNumber": "5656",    "cardData": {   "number": "<CardNum>",     "expiration": "1220"     }},     "transactionCode": "Force"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be Approved
    And response body path cvvResult should be P
    Examples:
      | AuthCode | CardNum          | amount | tipamount |
      | D6011    | 6011000993026909 | 10.01  | 1.50      |


  @post-transaction-credit_manual-no-avs
  Scenario Outline: Verify Transaction  'Credit' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",      "expiration": "1220"    }},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be <status>
    And response body path cvvResult should be <cvvResult>
    Examples:
      | txnCode | amount | cardNum         | status   | cvvResult |
      | Credit  | 16.00  | 371449635392376 | Approved | P         |


  @post-transaction-credit_swipe-with-avs
  Scenario Outline: Verify Transaction  'Credit' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "trackData":{    "value":"<trackvalue>",    "format":"ClearText",    "isContactless":false    },"billing":{"zip":"85284"}},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be <status>
    And response body path cvvResult should be <cvvResult>
    Examples:
      | txnCode | amount | trackvalue                                                        | status   | cvvResult |
      | Credit  | 9.00   | B5454545454545454^CHASE PAYMENTECH TEST CARD^20121015432112345601 | Approved | P         |


  @post-transaction-auth-capture_partial_rev
  Scenario Outline: Verify Transaction  'Auth' and  do a capture action as success.
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": <authAmnt>    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "<CardNum>",        "expiration": "1220"    },"billing":{"zip":"85284"},},     "transactionCode": "Authorization"}
    When I post data for transaction /BankCard/Transactions
    #Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be Approved
    And response body path cvvResult should be P
    Given I set Authorization header to Restaurant
    And I set body to {  "transactionCode": "Capture",  "amounts": {    "total": <settleAmnt>  }}
    When I patch data for transaction /BankCard/Transactions
    Then response code should be 200
    Examples:
      | authAmnt | settleAmnt | CardNum          |
      | 11.00    | 5.00       | 6011000993026909 |


  @post-transaction-auth-capture_full_rev
  Scenario Outline: Verify Transaction  'Auth' and  do a capture action as success.
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": <authAmnt>    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "<CardNum>",        "expiration": "1220"    },"billing":{"zip":"85284"},},     "transactionCode": "Authorization"}
    When I post data for transaction /BankCard/Transactions
        #Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be Approved
    And response body path cvvResult should be P
    Given I set Authorization header to Restaurant
    And I set body to {  "transactionCode": "Void"}
    When I patch data for transaction /BankCard/Transactions
    Then response code should be 200
    Examples:
      | authAmnt | CardNum         |
      | 7.05     | 371449635392376 |


  @post-settle-batch
  Scenario: Verify Settle batches
    Given I set Authorization header to Restaurant
    And I set content-type header to application/json
    And I set body to {  "settlementType": "Bankcard" }
    When I post data for transaction /BankCard/Batches/Current
    Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be Approved
    And response body path message should be BATCH 1 OF 1 CLOSED	