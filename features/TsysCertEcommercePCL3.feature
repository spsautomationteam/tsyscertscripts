Feature: E-commercePCL3 - Verify transaction like Auth,Sale, Credit or Force with cvvResult and Status for E-Commerce domain with testscope with\without AVS,refund,with/without CVV,credit,void,swipe/manual.


  @post-transaction-sale_manual-with-avs
  Scenario Outline: Verify Transaction  'Sale' transaction for set of  card numbers, amount and verify fo cvvResult and response
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",    "cvv":"<cvv>",   "expiration": "1220"    }, "billing":{"zip":"85284"},},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response body path status should be <status>
    And response header content-type should be application/json
    And response body path cvvResult should be <cvvResult>
    Examples:
      | txnCode | amount | cardNum          | status   | cvvResult | cvv  |
      | Sale    | 0.52   | 4055011111111111 | Approved | M         | 999  |
      | Sale    | 0.53   | 5525000000000054 | Approved | M         | 998  |
      | Sale    | 1.50   | 371449635392376  | Approved | M         | 9997 |
      | Sale    | 11.12  | 4012000098765439 | Approved | M         | 999  |
      | Sale    | 11.10  | 5499740000000057 | Approved | M         | 998  |
      | Sale    | 10.01  | 6011000993026909 | Declined | N         | 679  |
      | Sale    | 7.05   | 371449635392376  | Approved | M         | 9997 |


  @post-transaction-sale_manual-no-avs
  Scenario Outline: Verify Transaction  'Sale' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",    "cvv":"<cvv>",   "expiration": "1220"    }},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response body path status should be <status>
    Then response header content-type should be application/json
    Then response body path cvvResult should be <cvvResult>
    Examples:
      | txnCode | amount | cardNum          | status   | cvvResult | cvv |
      | Sale    | 6.00   | 3055155515160018 | Approved | M         | 996 |
      | Sale    | 10.00  | 6011000993026909 | Approved | M         | 996 |
      | Sale    | 9.67   | 3530142019945859 | Approved | M         | 996 |
      | Sale    | 8.88   | 2223000048400011 | Approved | M         | 998 |
      | Sale    | 4.00   | 5499740000000057 | Approved | M         | 998 |
      | Sale    | 10.10  | 6011000993026909 | Approved | M         | 996 |
      | Sale    | 3.50   | 4012000098765439 | Approved | M         | 999 |


  @post-transaction-sale_manual_cvv_mismatch-no-avs
  Scenario Outline: Verify Transaction  'Sale' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",    "cvv":"<cvv>",   "expiration": "1220"    }},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response body path status should be <status>
    Then response header content-type should be application/json
    Then response body path cvvResult should be <cvvResult>
    Then response body path message should be CVV2 MISMATCH
    Examples:
      | txnCode | amount | cardNum          | status   | cvvResult | cvv |
      | Sale    | 6.50   | 4012000098765439 | Declined | N         | 123 |


  @post-transaction-sale-recurring-manual-nocvv-with-avs
  Scenario Outline: Verify Transaction  'Sale' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to  {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",    "expiration": "1220"    }, "billing":{"zip":"85284"},    "isRecurring": true,     "recurringSchedule": {       "amount": <amount> ,       "frequency": "Monthly",       "interval": 1,       "nonBusinessDaysHandling": "After",       "startDate": "2017-08-30T05:05:00.546Z",       "totalCount": 2,          } 	  },     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response body path status should be <status>
    And response header content-type should be application/json
    And response body path cvvResult should be <cvvResult>
    Examples:
      | txnCode | amount | cardNum          | status   | cvvResult |
      | Sale    | 2      | 4012000098765439 | Approved | P         |

  @post-transaction-sale-recurring-manual-nocvv-no-avs
  Scenario Outline: Verify Transaction  'Sale' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to  {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",    "expiration": "1220"    },     "isRecurring": true,     "recurringSchedule": {       "amount": <amount> ,       "frequency": "Monthly",       "interval": 1,       "nonBusinessDaysHandling": "After",       "startDate": "2017-08-30T05:05:00.546Z",       "totalCount": 2,          } 	  },     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response body path status should be <status>
    And response header content-type should be application/json
    And response body path cvvResult should be <cvvResult>
    Examples:
      | txnCode | amount | cardNum          | status   | cvvResult |
      | Sale    | 3      | 5499740000000057 | Approved | P         |
      | Sale    | 4      | 6011000993026909 | Approved | P         |

  @post-transaction-auth-capture-recurring-manual-nocvv-no-avs
  Scenario Outline: Verify Transaction  'Sale' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to  {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",    "expiration": "1220"    },     "isRecurring": true,     "recurringSchedule": {       "amount": <amount> ,       "frequency": "Monthly",       "interval": 1,       "nonBusinessDaysHandling": "After",       "startDate": "2017-08-30T05:05:00.546Z",       "totalCount": 2,          } 	  },     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response body path status should be <status>
    And response header content-type should be application/json
    And response body path cvvResult should be <cvvResult>
    And I set body to {  "transactionCode": "Capture",  "amounts": {    "total": <capturamt>  }}
    When I patch data for transaction /BankCard/Transactions
    Then response code should be 200
    Examples:
      | txnCode       | amount | cardNum          | status   | cvvResult | capturamt |
      | Authorization | 11.10  | 4012000098765439 | Approved | P         | 5.55      |


  @post-transaction-auth-capture_partial_rev-with-avs
  Scenario Outline: Verify Transaction  'Auth' and  do a capture action as success.
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <authAmnt>    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "<CardNum>",        "expiration": "1220"    },"billing":{"zip":"85284"},},     "transactionCode": "Authorization"}
    When I post data for transaction /BankCard/Transactions
    And response header content-type should be application/json
    And response body path status should be Approved
    And response body path cvvResult should be P
    Given I set Authorization header to EcommercePcl3
    And I set body to {  "transactionCode": "Capture",  "amounts": {    "total": <settleAmnt>  }}
    When I patch data for transaction /BankCard/Transactions
    Then response code should be 200
    Examples:
      | authAmnt | settleAmnt | CardNum          |
      | 11.12    | 5.12       | 4012000098765439 |


  @post-transaction-auth-capture_full_rev-no-avs
  Scenario Outline: Verify Transaction  'Auth' full and  do a capture action as success.
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <authAmnt>    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "<CardNum>",        "expiration": "1220"    }},     "transactionCode": "Authorization"}
    When I post data for transaction /BankCard/Transactions
    And response header content-type should be application/json
    And response body path status should be Approved
    And response body path cvvResult should be P
    Given I set Authorization header to EcommercePcl3
    And I set body to {  "transactionCode": "Void"}
    When I patch data for transaction /BankCard/Transactions
    Then response code should be 200

    Examples:
      | authAmnt | CardNum          |
      | 4        | 5499740000000057 |


  @post-transaction-credit_manual-with-avs
  Scenario Outline: Verify Transaction  'Credit' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",      "expiration": "1220"    },"billing":{"zip":"85284"}},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be <status>
    And response body path cvvResult should be <cvvResult>

    Examples:
      | txnCode | amount | cardNum          | status   | cvvResult |
      | Credit  | 8.00   | 4012000098765439 | Approved | P         |

  @post-transaction-credit_manual-no-avs
  Scenario Outline: Verify Transaction  'Credit' transaction for set of  card numbers, amount and verify for cvvResult and response
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",      "expiration": "1220"    },},     "transactionCode": "<txnCode>"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be <status>
    And response body path cvvResult should be <cvvResult>

    Examples:
      | txnCode | amount | cardNum          | status   | cvvResult |
      | Credit  | 9.00   | 5499740000000057 | Approved | P         |


  @post-transaction-force-with-avs
  Scenario Outline: 'Force' transaction for set of  card numbers,  and verifycvvResult and response
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <amount>    },   "authorizationCode": "<AuthCode>",     "orderNumber": "5656",    "cardData": {   "number": "<CardNum>",     "expiration": "1220"     },"billing":{"zip":"85284"}},     "transactionCode": "Force"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be Approved
    And response body path cvvResult should be P
    Examples:
      | AuthCode | CardNum          | amount |
      | T12345   | 5499740000000057 | 3.33   |

  @post-transaction-force-no-avs
  Scenario Outline: 'Force' transaction for set of  card numbers,  and verifycvvResult and response
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "ECommerce": {   "amounts": {      "total": <amount>    },   "authorizationCode": "<AuthCode>",     "orderNumber": "5656",    "cardData": {   "number": "<CardNum>",     "expiration": "1220"     }},     "transactionCode": "Force"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be Approved
    And response body path cvvResult should be P
    Examples:
      | AuthCode | CardNum          | amount |
      | D60110   | 6011000993026909 | 10.01  |


  @post-settle-batch
  Scenario: Verify Settle batches
    Given I set Authorization header to EcommercePcl3
    And I set content-type header to application/json
    And I set body to {  "settlementType": "Bankcard" }
    When I post data for transaction /BankCard/Batches/Current
    #Then response code should be 201
    And response header content-type should be application/json
    And response body path status should be Approved
    And response body path message should be BATCH 1 OF 1 CLOSED
	