#This class handles all request response for Tsys scripts
class Tsyscert

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'rest_client'
  require "yaml"


  #@base_url
  @@reference_num = '1'
  @@content_type = ''
  @authorization=''

  #Constructor read yaml data
  def initialize()
    config = YAML.load_file('config.yaml')
    @base_url = config['url']
    @direct_marketing =config['DirectMarketing']
    @direct_marketing_pcl3 =config['DirectMarketingPcl3']
    @e_commerce =config['Ecommerce']
    @e_commerce_pcl3 =config['EcommercePcl3']
    @retail =config['Retail']
    @retail_debit =config['RetailDebit']
    @retail_pcl3 =config['RetailPcl3']
    @restaurant =config['Restaurant']
    puts "@base_url :#{@base_url}"
    puts "@direct_marketing #{@direct_marketing}"
    set_url @base_url
  end

  #Setter method for authorization
  def set_authorization(mid, mkey)
    @authorization ="Bearer SPS-DEV-GW:test."+mid+"."+mkey
  end

  #Setter method for url
  def set_url(url)
    @url = url
  end

  #Setter method for headers
  def set_header(header, value)
    case header
      when "content-type"
        @@content_type =value
      when "Authorization"
        case value
          when 'DirectMarketing'
            @authorization =@direct_marketing
          when 'DirectMarketingPcl3'
            @authorization=@direct_marketing_pcl3
          when 'Ecommerce'
            @authorization=@e_commerce
          when 'EcommercePcl3'
            @authorization=@e_commerce_pcl3
          when 'Retail'
            @authorization=@retail
          when 'RetailDebit'
            @authorization=@retail_debit
          when 'RetailPcl3'
            @authorization=@retail_pcl3
          when 'Restaurant'
            @authorization = @restaurant
          else
            @authorization =value
        end
    end
    puts "@authorization =:#{@authorization}"
  end


  #Setter method for json bosy
  def set_json_body(jsonbody)
    @json_body = jsonbody
  end

  #Http post method handler
  def post(path)
    http_requet("post", path)
  end

  #Http request handler for post, patch , get,delete
  def http_requet(method, path)
    full_url= @url+path

    #Debug urls
    puts "path#{path}"
    full_url= @url+path
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@@content_type: #{@@content_type}"
    puts "@@reference_num:#{@@reference_num}"

    begin
      case method
        when "post"
          @response = RestClient.post full_url, @json_body, :Authorization => @authorization, :content_type => @@content_type
          @@reference_num = JSON.parse(@response.body)['reference']
        when "patch"
          #if (!reference.empty?)
          full_url +='/'+@@reference_num
          @response = RestClient.patch full_url, @json_body, :Authorization => @authorization, :content_type => @@content_type
        when "get"
          @response = RestClient.get full_url, :Authorization => @authorization, :content_type => @@content_type
        when "delete"
          @response = RestClient.delete full_url, :Authorization => @authorization, :content_type => @@content_type
        when "put"
          @response = RestClient.put full_url, @json_body, :Authorization => @authorization, :content_type => @@content_type
      end
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end


    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
  end


  #Http patch request handler
  def patch(path)
    http_requet("patch", path)
  end

  #To verify response parameters
  def verify_response_params(response_param, value)
    #puts "response_param:#{response_param} , value:#{value}"
    if (response_param == "code")
      expect(@response.code.to_s).to eql(value), "Expected : #{value} ,got : #{@response.code.to_s} \nResponesbody:#{@parsed_response}"

    else
      if @parsed_response[response_param].nil?
        expect(@parsed_response[response_param]).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"

      else
        expect(@parsed_response[response_param].strip).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param].strip} \nResponesbody:#{@parsed_response}"
      end

    end
  end

  #To verify response headers
  def verify_response_headers(header_param, value)
    expect(@response.headers[:content_type]).to include(value), "Expected : #{value} ,got : #{@response.headers[:content_type]} \nResponesbody:#{@parsed_response}"
  end

end