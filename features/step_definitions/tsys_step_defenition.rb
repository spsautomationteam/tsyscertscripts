
And(/^I set url as (.*)$/) do |url|
  @tsyscert.set_url url
end

Given(/^I set (.*) header to (.*)$/) do |header, value|

  @tsyscert.set_header header, value
end

And(/^I set body to (.*)$/) do |json|
  @tsyscert.set_json_body json
end

And(/^I post data for transaction (.*)$/) do |path|
  @tsyscert.post path
end

And(/^I patch data for transaction (.*)$/) do |path|
  @tsyscert.patch path
end

And(/^response header (.*) should be (.*)$/) do |header_param, val|
  @tsyscert.verify_response_headers header_param, val
end

Then(/^response body path (.*) should be (.*)$/) do |param, val|
  @tsyscert.verify_response_params param, val
end

Then(/^response code should be (.*)$/) do |responsecode|
  @tsyscert.verify_response_params "code", responsecode
end